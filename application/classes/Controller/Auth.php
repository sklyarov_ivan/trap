<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Auth extends Controller_Layout {
    public $template = 'startup';
    protected $_user;
    protected $_auth;
    public function before() {
        parent::before();
        $this->_auth = Auth::instance();
        $this->_user = $this->_auth->get_user();
        $this->template->scripts = Arr::merge($this->template->scripts, array('addition/js/pages.js','addition/js/datapicker.js'));
        $this->template->styles = Arr::merge($this->template->styles, array('addition/css/jquery-ui.css'));
    }
    
    public function action_index()
    {
        echo Session::instance()->get('message', false);
        $this->response->body($this->action_login());
    }
    public function action_login()
    {
 
        if (Auth::instance()->logged_in())
        {
            HTTP::redirect('id-'.$this->_user->id);
        }
        if (!empty($_POST))
        {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $remember = isset($_POST['remember'])?(bool)$_POST['remember']:FALSE;
            if ($this->_auth->login($email, $password, $remember))
            {
               
                HTTP::redirect('id-'.Auth::instance()->get_user()->id);
            }
            $this->template->errors_login = 'wrong password or login';
            $this->template->data_login = $_POST;
        }
        
    }
    public function action_logout()
    {
        if ($this->_auth->logged_in())
        {
            $this->_auth->logout();
        }
    }
    
    public function action_registrate()
    {
        if (!empty($_POST))
        {
             foreach ($_POST as $key=>$val)
                {
                    $_POST[$key] = HTML::chars($val); 
                }
            try
            {
                $user = ORM::factory('user')
                        ->create_user($_POST,array('email','password','firstname','lastname','birthdate'))
                        ->add('roles', ORM::factory('Role', array('name'=>'login')));
               ;
                
                $this->action_login();
    
                

            }
            catch (ORM_Validation_Exception $e)
            {
                $errors = $e->errors('auth');
            }
            
        }
        
        $this->template->errors_registrate = isset($errors)?$errors:'';
        $this->template->data_registrate = $_POST;
    }
}