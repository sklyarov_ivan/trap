<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Ajax extends Controller {
    protected $_user;
    protected $_auth;
    public function before()
    {
        parent::before();
        $this->_auth = Auth::instance();
        $this->_user = $this->_auth->get_user();
    }
}