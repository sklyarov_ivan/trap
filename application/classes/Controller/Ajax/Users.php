<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Users extends Controller_Ajax_Ajax {
    public function before()
    {
        parent::before();
    }
    public function action_getall()
    {
        if (isset($_POST['user_query']))
        {
            $user_query = $_POST['user_query'];
        $part_user_name = explode(' ', $user_query);
       
        
        if (count($part_user_name) == 1)
            $users = ORM::factory('user')
                ->where_open()
                    ->where('firstname', 'LIKE', "%$part_user_name[0]%")
                    ->or_where('lastname', 'LIKE', "%$part_user_name[0]%")
                ->where_close()
                ->and_where('id', '!=', $this->_user->id)
                ;
        else
            $users = ORM::factory('user')
                ->where_open()
                    ->where('firstname', 'LIKE', "%$part_user_name[0]%")
                    ->and_where('lastname', 'LIKE', "%$part_user_name[1]%")
                ->where_close()
                ->or_where_open()
                    ->where('firstname', 'LIKE', "%$part_user_name[1]%")
                    ->and_where('lastname', 'LIKE', "%$part_user_name[0]%")
                ->or_where_close()
                ->and_where('id', '!=', $this->_user->id)
                ;
        
            $users_orm = $users->find_all();

                $all_find=array();
                $data = array();
                foreach ($users_orm as $user)
                {
                    $data['id'] = $user->id;
                    $data['firstname'] = $user->firstname;
                    $data['lastname'] = $user->lastname;
                    $data['image'] = $user->image;
                   $all_find[]=$data; 
                }
                echo json_encode($all_find);
        }
    }
}