<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Friends extends Controller_Ajax_Ajax {
    private $_current_userid;
    protected $_user;
    protected $_auth;
    
    public function before()
    {
        parent::before();
        $this->_current_userid = $this->request->param('id');
        $this->_auth = Auth::instance();
        $this->_user = $this->_auth->get_user();
        
    }
    public function action_index()
    {
        echo $this->_user->id;
    }
    public function action_accept_invitation()
    {
        $result = array('response'=>false);
        $user = ORM::factory('user', $this->_current_userid);
        if ($user->loaded())
        {
            if( Model::factory('User_Friend')->accept_invitation($this->_user->id,$this->_current_userid))
                $result = array('response'=>true);
            
    
        }
        echo json_encode($result);
        
    }
    
    public function action_delete_friend()
    {
         $result = array('response'=>false);
        $user = ORM::factory('user', $this->_current_userid);
        if ($user->loaded())
        {
            if( Model::factory('User_Friend')->delete_friend($this->_user->id,$this->_current_userid))
                $result = array('response'=>true);
            
    
        }
        echo json_encode($result);
        
    }
    public function action_invite_friend()
    {
        $result = array('response'=>false);
        $user = ORM::factory('user', $this->_current_userid);
        if ($user->loaded())
        {
            if( Model::factory('User_Friend')->invite_friend($this->_user->id,$this->_current_userid))
                $result = array('response'=>true);
            
    
        }
        echo json_encode($result);
    }
    

}