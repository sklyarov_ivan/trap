<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Auth extends Controller_Ajax_Ajax {
    
    protected $_user;
    protected $_auth;
    public $_response_array;
    public function before() {
        parent::before();
        $this->_auth = Auth::instance();
        $this->_user = $this->_auth->get_user();
        $this->response_array = array();
   }
    
    public function action_index()
    {
        echo $this;
    }
    public function action_login()
    {
        if (Auth::instance()->logged_in())
        {
            $this->response_array['redirect'] = 'id-'.$this->_user->id;
        }
        else
        {
            if (!empty($_POST))
            {
                $email = $_POST['email'];
                $password = $_POST['password'];
                $remember = isset($_POST['remember'])?(bool)$_POST['remember']:FALSE;
                if ($this->_auth->login($email, $password, $remember))
                {
                    $this->_response_array['redirect'] = 'id-'.Auth::instance()->get_user()->id;
                }  else {    
                    $this->_response_array['errors'] = 'wrong email or password';
                }
            }  
            echo json_encode($this->_response_array);
        }
        
    }
    public function action_logout()
    {
        if ($this->_auth->logged_in())
        {
            $this->_auth->logout();
        }
    }
    
    public function action_registrate()
    {
        if (!empty($_POST))
        {
            try
            {
                foreach ($_POST as $key=>$val)
                {
                    $_POST[$key] = HTML::chars($val); 
                }
                $user = ORM::factory('user')
                        ->create_user($_POST,array('email','password','firstname','lastname','birthdate'))
                        ->add('roles', ORM::factory('role', array('name'=>'login')));
                $this->action_login();
    
                
            }
            catch (ORM_Validation_Exception $e)
            {
                $this->response_array['errors'] = $e->errors('auth');
            }
            
        }
             echo json_encode($this->response_array);
        
    }
    
    public function __toString() {
        return Request::initial()->controller();
    }
    
    public function action_checkemail()
    {
        
        header('Content-type: application/json');
        $response = array('email'=>'');
        if (isset($_POST['email']))
        {
            $checkemail = ORM::factory('user')->where('email','=',$_POST['email'])->find();
            
            if (isset($checkemail->email))
            {

                echo $response['email'] = 'false';
            }
            else
            {
                echo $response['email'] = 'true';
            }
//            echo json_encode($response['email']);
        }
    }
}