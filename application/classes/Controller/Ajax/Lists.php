<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Lists extends Controller_Ajax_Ajax {
    private $_curent_page;
    
    public function before()
    {
        parent::before();
        $this->_current_page = $this->request->param('id');
        
    }
    public function action_userlist()
    {
        $list = View::factory('ajax/ajax_userlist')
                ->bind('users', $pag_user)
                ->bind('pagination',$pagination);
        $users = ORM::factory('user')->where('id','!=',$this->_current_page)->find_all();
        
        $count =$users->count();
        $pagination = Pagination::factory(array('total_items' => $count))
                ->route_params(array(
                    'controller'    => Request::current()->controller(),
                    'action'        => Request::current()->action()
                ));
                
        $pagination->current_page = $this->_current_page;
        $pag_user = ORM::factory('user')
        ->limit($pagination->items_per_page)
        ->offset($pagination->offset)
        ->find_all();

        echo $list;
    }
}