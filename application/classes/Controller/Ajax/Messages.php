<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax_Messages extends Controller_Ajax_Ajax {
    protected $_user;
    protected $_auth;
    public function before()
    {
        parent::before();
        $this->_auth = Auth::instance();
        $this->_user = $this->_auth->get_user();
    }
    public function action_posted()
    {
        $friends = ORM::factory('user', $this->_user->id)->post_messages;
        $count = $friends->find_all()->count();
        $pagination = Pagination::factory(array('total_items' => $count, 'current_page'=>'query_string',
                                            ))
                ->route_params(array(
                    'controller'    => Request::current()->controller(),
                    'action'        => Request::current()->action()
                ));
        
        $pagination->current_page = $this->request->param('id');
        $result = Model::factory('User_Message')->get_posted($pagination->items_per_page, $pagination->offset, $this->_user->id);

        $content = View::factory('ajax/messages_posted')
                ->bind('messages', $result)
                ->bind('owner', $this->_user)
                ->bind('pagination', $pagination);
        echo $content;
    }
    public function action_inbox()
    {
        $friends = ORM::factory('user', $this->_user->id)->inbox_messages;
        $count = $friends->find_all()->count();
        $pagination = Pagination::factory(array('total_items' => $count, 'current_page'=>'query_string',
                                            ))
                ->route_params(array(
                    'controller'    => Request::current()->controller(),
                    'action'        => Request::current()->action()
                ));
        $pagination->current_page = $this->request->param('id');
        $result = Model::factory('User_Message')->get_inbox($pagination->items_per_page, $pagination->offset, $this->_user->id);
        $content = View::factory('ajax/messages_inbox')
                ->bind('messages', $result)
                ->bind('owner', $this->_user)
                ->bind('pagination', $pagination);
        echo $content;
    }
    public function action_sand()
    {
        if ($this->request->method('POST'))
        {
            try{
            ORM::factory('User_Message')
                    ->values(array(
                        'sender_id'     =>  (int) $_POST['from'],
                        'recipient_id'  =>  (int) $_POST['to'],
                        'title'         =>  HTML::chars($_POST['title']),
                        'message'       =>  trim(HTML::chars($_POST['message'])),
                        'time'          => date("Y-m-d H:i:s",time()),
                        'read_key'      =>  0
                    ))->save();
           echo true;
            }
           catch (ORM_Validation_Exception $e)
           {
               echo $e->errors();
           }
            
        }
    }
}