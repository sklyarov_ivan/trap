<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Layout extends Controller_Template {
    public $template = 'layout';
    protected $_auth;
    protected $_user;


    public function before()
    {
        parent::before();
        
        $this->_auth = Auth::instance();
        $this->_user = $this->_auth->get_user();
        $this->template->title_page = '';
        $this->template->title = Kohana::$config->load('base')->get('title').$this->template->title_page;
        
        $this->template->styles = Kohana::$config->load('links')->get('styles');
        $this->template->scripts = Kohana::$config->load('links')->get('scripts');
        
        $this->template->header = array();
        $this->template->footer = array();
        $this->template->container_left = array();
        $this->template->container_center = array();
        $this->template->container_right = array();
    }
}