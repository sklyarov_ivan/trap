<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Users_Registers extends Controller_Users_Users {
    protected $_auth;
    protected $_user;
    protected $_curent_userid;
    protected $_request_friend_id;
    private $_page;
    public function before()
    {
        parent::before();
        $this->_auth = Auth::instance();
        $this->_user = $this->_auth->get_user();
        $this->_curent_userid = $this->_user->id;
        $this->_request_friend_id = $this->request->param('codeid');
        $this->_page = $this->request->param('page');
    }
    
    public function action_userlist()
    {
        $this->template->scripts = Arr::merge($this->template->scripts, array('addition/js/msg.js'));
        $list = View::factory('users/userlist')
                ->bind('users', $pag_user)
                ->bind('pagination',$pagination);
        $users = ORM::factory('user')->where('id','!=',$this->_curent_userid)->find_all();
        
        $count =$users->count();
        $pagination = Pagination::factory(array('total_items' => $count))
                ->route_params(array(
                    'controller'    => Request::current()->controller(),
                    'action'        => Request::current()->action()
                ));
    
    
        $pag_user = ORM::factory('user')
        ->where('id','!=',$this->_curent_userid)
        ->limit($pagination->items_per_page)
        ->offset($pagination->offset)
        ->find_all();

        
        $this->template->container_center = array($list);
    }
    
  
    public function action_friends()
    {
        $users = array();
        $access = array();
        
        $content = View::factory('users/friendlist')
                ->bind('users', $users_to_form)
                ->bind('curentuser', $curentuser)
                ->bind('access', $access)
                ->bind('pagination', $pagination);
        $friends = ORM::factory('user', $this->_user->id)->friends;
        
        $count = $friends->find_all()->count();
        $pagination = Pagination::factory(array('total_items' => $count))
                ->route_params(array(
                    'controller'    => Request::current()->controller(),
                    'action'        => Request::current()->action()
                ));
    
    
        $total_friends = ORM::factory('user', $this->_user->id)
        ->friends
        ->order_by('access_token','ASC')
        ->limit($pagination->items_per_page)
        ->offset($pagination->offset)
        ->find_all();
        
        foreach ($total_friends as $friend)
        {
           $var = ORM::factory('user',$friend->friend_id);
           array_push($users, $var);
           $access[$friend->friend_id] = $friend->access_token;
        }
        
        $users_to_form = (object) $users;
        $curentuser = $this->_curent_userid;
      
        $this->template->container_center = array($content);
    }
    
    
    
 
}