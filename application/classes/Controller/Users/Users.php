<?php defined('SYSPATH') or die('No direct script access.');


abstract class Controller_Users_Users extends Controller_Layout {
    public function before()
    {
        parent::before();
        
          if (!Auth::instance()->logged_in())
            {
                HTTP::redirect('/');
            }
        
        $this->template->styles = Arr::merge($this->template->styles, array('addition/css/autocomplete.css'));
        $this->template->scripts = Arr::merge($this->template->scripts, array('addition/js/autocomplete.js',
                                                                               'addition/js/pages.js'));
//headmenu
        $headmenu = Request::factory('widget/Headmenu')->execute()->body();
        $this->template->header = array($headmenu);
//usermenu
        $user_menu = Request::factory('widget/Usermenu')->execute()->body();
        $this->template->container_left = array($user_menu);
       
    }
} 