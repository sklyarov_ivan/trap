
<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Users_Messages extends Controller_Users_Users {
       protected $_user;
       protected  $_auth;
       public function before() {
            parent::before();
            $this->_auth = Auth::instance();
            $this->_user = $this->_auth->get_user();
            $this->template->scripts = Arr::merge($this->template->scripts, array('addition/js/messages.js'));
        }
	public function action_index()
	{
            $content = View::factory('users/messages');
          $this->template->container_center = array($content);
	}
        
        public function action_read()
        {
            $msg_id = $this->request->param('id') ;
            if ($message=Model::factory('User_Message')->check_message_id($msg_id, $this->_user->id) )
            {
                
                $content = View::factory('messages/message_read')
                        ->bind('message', $message);
            $this->template->container_center = array($content);
            }else{
                throw new HTTP_Exception_404;
            }
        }

} // End Welcome
