<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Users_Profile extends Controller_Users_Users {
    /**
     *
     * @var obj 
     */
    protected $_user;
    /**
     *
     * @var obj 
     */
    protected $_auth;
    /**
     *
     * @var int 
     */
    protected $_current_userid;
    
    public function before()
    {
        parent::before();
       $this->_auth = Auth::instance();
       $this->user = $this->_auth->get_user();
       $this->_current_userid = $this->request->param('codeid');
       $user = ORM::factory('user', $this->_current_userid);
       if (!$user->loaded())
            throw new HTTP_Exception_404;
    
      
    }
    /**
     * 
     */
    public function action_index()
    {
        $this->template->scripts = Arr::merge($this->template->scripts, array('addition/js/msg.js'));
        $all_records =array();
        $profile = View::factory('users/user')
                ->bind('user', $user)
                ->bind('current_user', $this->_user)
                ->bind('user_status',$friend_status);
       
        $user = ORM::factory('user', $this->_current_userid);
        
        if ($this->_current_userid != $this->_user->id)
            $friend_status = Model::factory('User_Friend')->friend_status($this->_user->id, $this->_current_userid);
        $this->template->container_center = array($profile);
      
    }
    /**
     * @var string firstname by $_POST
     * @var string lastname by $_POST
     * @var string emailname by $_POST
     * @var string birthdate by $_POST
     * @var resounrce image by $_POST
     */
    public function action_editprofile()
    {
        $this->template->scripts = Arr::merge($this->template->scripts, array('addition/js/datapicker.js'));
        
      
       
        if (isset($_POST['email']))
        {
            foreach ($_POST as $key=>$val)
            {
                $_POST[$key] = HTML::chars($val); 
            }
            $user = ORM::factory('user', $this->_user->id);

            try
            {
                
                $validate = Validation::factory($_FILES);
                $validate->rules('image',
                        array(
                              'Upload::type' =>array('Upload::type', array('jpg','png','gif')),
                              'Upload::size' => array('1M'))
                );
 
                
//                if ($validate->check())
//                {
                        $file = $this->_add_image($_FILES['image']['tmp_name']);
//                }
//                else
//                {
                        // Вывод ошибки
                        $errors = $validate->errors('upload');
                        print_r($errors);
//                }
                
                
                
                
               
                $update_data = array(
                    'firstname' => $_POST['firstname'],
                    'lastname' => $_POST['lastname'],
                    'email' => $_POST['email'],
                    'birthdate' => $_POST['birthdate']
                         );
                if (!empty($file))
                    $update_data['image'] = $file;
                $user->values($update_data)
                        ->save();
                HTTP::redirect('/id-'.$this->_user->id.'/editprofile');
            }
            catch (ORM_Validation_Exception $e)
            {
                $errors = $e->errors();
            }

        }
        if (isset($_POST['password']))
        {

        }
  
        $content = View::factory('users/profile')
                ->bind('user', $this->_user)
                ->bind('errors', $errors);
        $this->template->container_center = array($content);
    }
    private function _add_image($filename,$ext=NULL,$directory=NULL)
    {
        if ($ext === NULL)
        {
            $ext = 'png';
        }
        if ($directory === NULL)
        {
            $directory = 'addition/img/upload/';
        }
        $new_name = time().'_'.$this->_user->id;
        $img = Image::factory($filename)
                ->resize(200, 200, Image::AUTO)
            ->save($directory.$new_name.'.'.$ext);
        return $new_name;
    }
    public function action_new_record()
    {
         $records = ORM::factory('User_Record');
        $try_get_records = $records->where('recipient_id','=',$this->_user->id)->find();
        if (!$try_get_records->id)
        {
            $records->make_root();
        }
                
    }
    public function action_answer_record()
    {
        
    }

    
    
    
    
    
    
  
    
    
}