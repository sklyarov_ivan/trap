<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Widget_Usermenu extends Controller_Widgets {

    public $template = 'widgets/user_menu';
    public function action_index()
    {
      $this->template->user = Auth::instance()->get_user();
      $friends = ORM::factory('user', $this->template->user->id)->friends;
      $active_friends = $friends->where('access_token','=','1')->count_all();
      $inactive_friends = $friends->where('user_id','=',$this->template->user->id)->where('access_token','=','0')->count_all();
      
      $this->template->active = $active_friends;
      $this->template->inactive = $inactive_friends;
    }
}