<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Widget_Profile extends Controller_Widgets {

    public $template = 'widgets/messages_form';
    public function action_index()
    {

        $this->template->messages = Model::factory('User_Message')->get_posted($this->_user->id);

    }
}