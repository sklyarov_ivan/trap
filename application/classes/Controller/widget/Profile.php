<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Widget_Profile extends Controller_Widgets {

    public $template = 'widgets/profile';
    public function action_index()
    {
      $this->template->user = Auth::instance()->get_user();
    }
}