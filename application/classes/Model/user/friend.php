<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_User_Friend extends ORM {
    protected $_table_name = 'user_friends';
    private $user = 'user_id';
    private $friend = 'friend_id';
  
    public function accept_invitation($account_owner,$friend)
    {
        try
        {
    
          $current_chain=ORM::factory('User_Friend')->where('user_id','=',$account_owner)
                        ->and_where('friend_id', '=', $friend)->find();
            
                $friend_accept_invitation =ORM::factory('User_Friend');
                $friend_accept_invitation->values(array(
                    'user_id' => $friend,
                    'friend_id' => $account_owner,
                    'access_token'  =>  1,
                ))->save();
                ORM::factory('User_Friend', $current_chain)
                        ->values(array('access_token' => 1))
                        ->save();
                return true;
        }catch(Kohana_Validation_Exception $e)
        {
            return false;
        }
    }
    
    public function delete_friend($account_owner,$friend)
    {
        try
        {
    
          $chains = ORM::factory('User_Friend')
                  ->where_open()
                      ->where('user_id', '=', $account_owner)
                      ->and_where('friend_id', '=', $friend)
                  ->where_close()
                  ->or_where_open()
                      ->where('user_id', '=', $friend)
                      ->and_where('friend_id', '=', $account_owner)
                  ->or_where_close()->find_all();

          
          foreach($chains as $chain)
            {
                    $chain->delete();
            }
                return true;
        }catch(Kohana_Validation_Exception $e)
        {
            return false;
        }
    }
    public function invite_friend($account_owner,$friend)
    {
         try
        {
    
                $friend_accept_invitation =ORM::factory('User_Friend');
                $friend_accept_invitation->values(array(
                    'user_id' => $friend,
                    'friend_id' => $account_owner,
                    'access_token'  =>  0,
                ))->save();
                
                return true;
        }catch(Kohana_Validation_Exception $e)
        {
            return false;
        }
    }
    /**
     * check user status
     *  friend
     *  person, who invite account owner
     *  unknown person
     *  not_accept
     * 
     * @param int $account_owner
     * @param int $friend 
     */
    public function friend_status($account_owner,$friend)
    {
        $case1 = ORM::factory('User_Friend')
                      ->where('user_id', '=', $account_owner)
                      ->and_where('friend_id', '=', $friend)
                      ->and_where('access_token', '=', 1)
                      ->find();
                
        $case2 = ORM::factory('User_Friend')
                      ->where('user_id', '=', $account_owner)
                      ->and_where('friend_id', '=', $friend)
                      ->and_where('access_token', '=', 0)
                      ->find();
        $case3 =  ORM::factory('User_Friend')
                      ->where('user_id', '=', $friend)
                      ->and_where('friend_id', '=', $account_owner)
                      ->and_where('access_token', '=', 0)
                      ->find();
          
        if ($case1->id)
            return 'friend';
        echo ($case1->id);
        if ($case2->id)
            return 'invite';
        if ($case3->id)
            return 'not_accept';
        return 'unknown';
        
    }
    
    
 

    
    
    
}