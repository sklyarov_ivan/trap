<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_User_Record extends ORM_MPTT {
    public function rules()
    {
        return array(
            'mesage' => array(
                array('not_empty'),
            )
        );
    }
    
    public function filters()
    {
        return array(
            TRUE => array(
                array('trim')
            ),
            'message' => array(
                array('strip_tags')
            )
        );
    }
}