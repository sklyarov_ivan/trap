<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_User_Message extends ORM {
    protected $_table_name = 'user_messages';
    public function check_message_id($msg_id, $user_id)
    {
        $msg = ORM::factory('User_Message')
                ->where('id', '=', $msg_id)
                ->and_where_open()
                    ->where('sender_id', '=', $user_id)
                    ->or_where('recipient_id', '=', $user_id)
                ->and_where_close()
                ->find();
        if ($msg->id)
        {
            return $msg;
        }
        return false;
    }
    public function get_inbox($items_per_page, $offset, $userid)
    {
        $pag_message = ORM::factory('User_Message')
            ->where('recipient_id','=',$userid)
            ->limit($items_per_page)
            ->offset($offset)
            ->find_all();
        $result=array();
        foreach($pag_message as $mes_user)
        {
            $user = ORM::factory('user', $mes_user->sender_id);
            $data=array();
            $data['user'] =  $user;
            $data['message'] = $mes_user;
            $result[]=$data;
        }
            return $result;
    }
    
    public function get_posted($items_per_page, $offset, $userid)
    {
        $pag_message = ORM::factory('User_Message')
            ->where('sender_id','=',$userid)
            ->limit($items_per_page)
            ->offset($offset)
            ->find_all();
        $result=array();
        foreach($pag_message as $mes_user)
        {
            $user = ORM::factory('user', $mes_user->sender_id);
            $data=array();
            $data['user'] =  $user;
            $data['message'] = $mes_user;
            $result[]=$data;
        }
            return $result;
    }
    
}