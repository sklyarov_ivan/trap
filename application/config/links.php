<?php defined('SYSPATH') or die('No direct script access.');
$styles = 'addition/css/';
$scripts = 'addition/js/';
return (array(
    'styles'    =>  array(
        $styles.'bootstrap.min.css',
        $styles.'bootstrap-responsive.min.css',
        $styles.'bootswatch.css',
        $styles.'jquery-ui.css'
    ),
    'scripts'   =>  array(
//        $scripts.'bsa.js',
        $scripts.'jquery.1.9.1.min.js',
        $scripts.'bootstrap.min.js',
        $scripts.'application.js',
        $scripts.'bootswatch.js',
        $scripts.'jquery-ui.js',
        $scripts.'jquery.validate.min.js',
        $scripts.'myvalidate.js',
    )
));