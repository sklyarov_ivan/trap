<?php defined('SYSPATH') or die('No direct script access.');


return array(
    'title' =>  'TRAP the social network :: ',
    'records_per_page'  =>  15,
    'site_name' =>  'T&#8476;&cap;P'
);