
<div class="modal hide" id="msgModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">✕</button>
        <h3>Send message</h3>
    </div>
        <div class="modal-body" style="text-align:center;">
        <div class="row-fluid">
            <div class="span10 offset1">
                <div id="modalTab">
                    <div class="tab-content">
                        
                            <form method="post" action='/messages/send' name="login_form">
                                <p><input type="text" class="span10" name="title" id="title" placeholder="Title"></p>
                                <p><textarea rows="3" class="span10" id="message" name="message" class="input-block-level" placeholder="Your message"></textarea></p>
             
                                <p><input type="submit" class="btn btn-primary" value="Send">
                                <button class="btn-info" data-dismiss="modal">Cancel</button>
                                </p>
                            </form>
                        
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>