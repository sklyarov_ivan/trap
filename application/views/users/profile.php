<div class="well" >
    <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">Profile</a></li>
      <li><a href="#profile" data-toggle="tab">Password</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
       <div id="tab">
        <form id="registrate_form" enctype="multipart/form-data" action="<?php echo $_SERVER['REQUEST_URI']?>" method="post">
           
            <label>First Name</label>
            <input type="text" name="firstname" value="<?php echo $user->firstname?>" class="input-xlarge">
            <label>Last Name</label>
            <input type="text" name="lastname" value="<?php echo $user->lastname?>" class="input-xlarge">
            <label>Email</label>
            <input type="text" name="email" value="<?php echo $user->email?>" class="input-xlarge">
            <label>Birth Date</label>
            <input type="text" name="birthdate" id="birthdate" value="<?php echo $user->birthdate?>" class="input-xlarge">
            <label>Avatar</label>
            <input type="file" name="image" value="" class="input-xlarge">
            
            <label>About Me</label>
            <textarea value="Smith" name="about" rows="6" class="input-xlarge">
            
            </textarea>
          	<div>
        	    <button class="btn btn-primary">Update</button>
        	</div>
        </form>
        </div>
      </div>
      <div class="tab-pane fade" id="profile">
    	<form id="tab2">
        	<label>New Password</label>
        	<input type="password" class="input-xlarge">
        	<div>
        	    <input type="submit" class="btn btn-primary" value="Update">
        	</div>
    	</form>
      </div>
  </div>