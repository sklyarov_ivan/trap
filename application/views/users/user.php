<div class=" well row">
            <div class="span6">
            <div class="logowrapper">
                <img class="logoicon" src="addition/img/upload/<?php echo !empty($user->image)?$user->image:'user'?>.png" alt="App Logo"/>
                <?php if (isset($user->id) AND (isset($current_user->id) AND ($user->id != $current_user->id))):?>
                <a data-toggle="modal" href="#msgModal"><span class="badge badge-info">send a message</span></a>
                    <?php if ($user_status != 'not_accept' AND $user_status == 'unknown'):?>
                        <a id="add-to-friend" href="/invite_friend/id-<?php echo $user->id?>"><span class="badge badge-warning">add to friend</span></a>
                
                    <?php elseif($user_status == 'invite'):?>
                        <a id="accept-friend" href="/accept_invitation/id-<?php echo $user->id?>"><span class="badge badge-important">accept invitation</span></a>
                    <?php elseif($user_status == 'friend'):?>
                        <a id="delete-friend" href="/delete_friend/id-<?php echo $user->id?>"><span class=" badge badge-inverse">delete from user list</span></a>
                    <?php endif;?>
                <?php endif;?>
            </div>
            </div>
            <div class="span6">
                <form class="form-horizontal">
                    <p class="help-block"><b>Name:</b>       <span><?php echo ucfirst($user->firstname).' '.ucfirst($user->lastname)?></span></p>
                    
                  
                    
                   
                    <div class="help-block">
                        <p class="help-block"><b>Birth Date:</b> <span><?php echo $user->birthdate?></span></p>
                       
                    </div>
                </form>
            </div>
        </div>

 
<div class="modal hide" id="msgModal">
    <div class="modal-header">
        <button type="button" id="close_modal_msg" class="close" data-dismiss="modal">✕</button>
        <h4>send message to <?php echo ucfirst($user->firstname).' '.ucfirst($user->lastname)?></h4>
    </div>
        <div class="modal-body" style="text-align:center;">
        <div class="row-fluid">
            <div class="span10 offset1">
                <div id="modalTab">
                    <div class="tab-content">
                        <div class="tab-pane active" id="message">
                            <form method="post" action='' name="message">
                                <p><input type="hidden" name="to" value="<?php echo $user->id?>"></p>
                                <p><input type="hidden" name="from" value="<?php echo $current_user->id;?>"></p>
                                <p><input type="text" class="span10" name="title" id="title" placeholder="Title"></p>
                                <p>
                                <textarea class="span10" placeholder="Message" name="message" rows="5" class="input-xlarge"></textarea></p>
                                <p><button type="submit" class="btn btn-info">Send</button>
                                    <button type="submit" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                </p>
                            </form>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>