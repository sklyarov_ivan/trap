<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title><?php echo $title?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Optimized for legibility.">
        <meta name="author" content="Thomas Park">

        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!--css:start -->
        <?php foreach($styles as $style):?>
            <?php echo HTML::style($style);?>
        <?php endforeach;?>
        <!--css:end -->
<!--        <script type="text/javascript">

         var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-23019901-1']);
          _gaq.push(['_setDomainName', "bootswatch.com"]);
            _gaq.push(['_setAllowLinker', true]);
          _gaq.push(['_trackPageview']);

         (function() {
           var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
           ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
           var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();

        </script>-->
  </head>
  <body class="preview" data-spy="scroll" data-target=".subnav" data-offset="80">
   
      <div class="container">
          <div align="center">
            <div class="span-4 offset3">
                  <h1>TRAP</h1>
                  <h4>"not ever think to get out the trap"
                  <h5 >The social network</h5></h4>
            </div>   
    <div class="span4 offset3">

      
      <ul class="nav nav-tabs">
        <li class="active"><a href="#login" data-toggle="tab">Log In</a></li>
        <li><a href="#registrate" data-toggle="tab">Registrate</a></li>
      </ul>
      <div class="tabbable">
        <div class="tab-content">
          <div class="tab-pane active" id="login">
           
                <div class="error" style="display: 'none'"><?php echo isset($errors_login)?$errors_login:''?></div>
            <form method="post" action='<?php echo Route::get('auth')->uri(array('action'=>'login'))?>' name="login_form" id="login_form">
                <p><input type="text" class="span3" name="email" id="email" placeholder="Email" value="<?php echo isset($data_login['email'])?$data_login['email']:''?>"><span id="val_msg"></span></p>
                <p><input type="password" class="span3" name="password" placeholder="Password" value="<?php echo isset($data_login['password'])?$data_login['password']:''?>"><span id="val_msg"></span></p>
                
                    <p><input type="checkbox" name="remember" value="1"> Remember Me</p>
                <p><input type="submit" class="btn btn-primary" value="Sign in">
                </p>
            </form>
                   
      
          </div>
          <div class="tab-pane" id="registrate">
              
              <form method="post" action='<?php echo Route::get('auth')->uri(array('action'=>'registrate'))?>' name="registrate_form" id="registrate_form">           
                 <p><input type="text" class="span3" name="firstname" placeholder="First Name" value="<?php echo isset($data_registrate['firstname'])?$data_registrate['firstname']:''?>"></p>
                <p><input type="text" class="span3" name="lastname" placeholder="Last Name" value="<?php echo isset($data_registrate['lastname'])?$data_registrate['lastname']:''?>"></p>
                <p><input type="text" class="span3" id="birthdate" name="birthdate" placeholder="Birth Date" value="<?php echo isset($data_registrate['birthdate'])?$data_registrate['birthdate']:''?>"></p>
                <!--<p><input type="file" class="span3" name="yourphoto" placeholder="Your photo" value="<?php echo isset($data_registrate['yourphoto'])?$data_registrate['yourphoto']:''?>"></p>-->
                <p><input type="text" class="span3" name="email" id="email" placeholder="Email" value="<?php echo isset($data_registrate['email'])?$data_registrate['email']:''?>"></p>
                <p><input type="password" class="span3" name="password" id="password" placeholder="Password"></p>
                <p><input type="password" class="span3" name="password_confirm" placeholder="Confirm Password"></p>
                
                <p><input type="submit" class="btn btn-primary" id="btn_reg"value="Registrate">
                </p>
            </form>
          </div>
         
        </div>
      </div> <!-- /tabbable -->
      
      </div> <!-- container:end-->

          </div>
          


        <!--js:start-->
        <?php foreach($scripts as $script):?>
          <?php echo HTML::script($script);?>
        <?php endforeach;?>
        <!--js:end-->
  </body>
</html>
