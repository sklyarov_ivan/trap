<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <title><?php echo $title?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Optimized for legibility.">
        <meta name="author" content="Thomas Park">

        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!--css:start -->
        <?php foreach($styles as $style):?>
            <?php echo HTML::style($style);?>
        <?php endforeach;?>
        <!--css:end -->
       
  </head>
  <body class="preview" data-spy="scroll" data-target=".subnav" data-offset="80">
        <?php foreach($header as $single_header):?>
            <?php echo $single_header?>
        <?php endforeach;?>
   
      <div class="container">
  <div class="row-fluid">
    <div class="span2">
      <!--Sidebar content-->
                <?php foreach($container_left as $single_container_left):?>
                    <?php echo $single_container_left;?>
                <?php endforeach;?>
    </div>
    <div class="span8 offset1" id="content_container">
      <!--Body content-->
          `<?php foreach($container_center as $single_container_center):?>
                <?php echo $single_container_center;?>
            <?php endforeach;?>
    </div>
  </div>
</div>
      
        <div class="row-fluid showgrid">
            <div class="span-4">
            </div>
            <div class="span-5 offset4">
            </div>
        </div>
        <!-- Navbar
        ================================================== -->
       

        <div class="container">


        




        </div><!-- /container -->

         <?php foreach($footer as $single_footer):?>
            <?php echo $single_footer?>
        <?php endforeach;?>

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <!--js:start-->
        <?php foreach($scripts as $script):?>
          <?php echo HTML::script($script);?>
        <?php endforeach;?>
        <!--js:end-->
  </body>
</html>
