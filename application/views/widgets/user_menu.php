

<div class="sidebar-nav">
	<div class="well" style="width:200px; padding: 8px 0; float: left;">
		<ul class="nav nav-list"> 
                    <li class="nav-header">User Menu</li>        
                    <li><a href="<?php echo '/id-'.$user->id?>"><i class="icon-home"></i> My page</a></li>
                    <li><a href="/messages"><i class="icon-envelope"></i> Messages </a></li>
                    <li><a href="/friends"><i class="icon-user"></i> Friends  <span class="badge badge-info"><?php echo $active?></span>
                        <span class="badge badge-warning"><?php echo $inactive?></span></a>
                      
                    </li>
                   
                    <li class="divider"></li>
                    <li><a href="<?php echo '/id-'.$user->id.'/editprofile'?>"><i class="icon-exclamation-sign"></i> Settings</a></li>
                    <li><a href="/logout"><i class="icon-share"></i> Logout</a></li>
		</ul>
	</div>
</div>