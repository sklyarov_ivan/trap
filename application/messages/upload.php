<?php defined('SYSPATH') or die('No direct script access.');

return array
(
	'picture' => array(
	'Upload::valid'    	=> 'upload error',
    	'Upload::not_empty'    	=> 'image must be not empty',
    	'Upload::type'    	=> 'wrong image type',
    	'Upload::size'    	=> 'too much big size og image',
    	'default'  		=> 'error'),
);