$(function() {
    
    var search_field = $( "#search_user" );
    search_field
    .autocomplete({
      source: function(request, response)
      {
          $.ajax({
              url: 'http://'+document.location.hostname+'/ajax/users/getall',
              type: 'POST',
              data: 'user_query='+search_field.val(),
              dataType: 'json',
              success: function(data)
              {
                  console.log(data);
                  var dataarray = $.makeArray(data);
                  response($.map(dataarray, function(item){
                      if (item.image == '')
                          {item.image = 'user'}
              return{
                  
                    label: item.firstname +' '+item.lastname , 
                    value: item.firstname +' '+item.lastname,
                    url: '/id-'+item.id,
                    avatar: "/addition/img" + item.image+'.png'
              }
                  }));
              },
              error: function(a,b,c)
              {
                  console.log(a+b+c);
              }
          })
      }, 
        messages: {
        noResults: null,
        results: function() {}
        },
      select: function(event, ui)
      {
          window.location.href = ui.item.url;
      }
    })
//    .addClass( "ui-state-default ui-combobox-input" )
//    .data("autocomplete")._renderItem = function(ul, item) {
//        return $("<li />")
//            .data("item.autocomplete", item)
//            .append("<a><img src='" + item.avatar + "' />" + item.value + "</a>")
//            .appendTo(ul);
//    };
 
    
   
    
    
  });