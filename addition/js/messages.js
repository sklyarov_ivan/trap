var containerId = '#tabs-container'; 
var tabsId = '#message_tags'; 

$(document).ready(function(){     // Preload tab on page load    
  
    if($(tabsId + ' LI.active a').length > 0)
    {
        loadTab($(tabsId + ' LI.active a'));     
    }         
    $(tabsId + ' a').click(function(){         
    if($(this).parent().hasClass('active')){ return false; }                 
    $(tabsId + ' LI.active').removeClass('active');         
    $(this).parent().addClass('active');                 
    loadTab($(this));                
    return false;     
    }); 
    
    $('#read_message').on('click',function(event){
        event.preventDefault();
        console.log('read message');
    });
    
    
});
    
    function loadTab(tabObj){     
    if(!tabObj || !tabObj.length){ return; }     
//    $(containerId).addClass('loading');     
    $(containerId).fadeOut('fast');
    
    $(containerId).load('/ajax'+tabObj.attr('href'), function(){         
//    $(containerId).removeClass('loading');         
    $(containerId).fadeIn('fast'); 
    
    }); 

}


