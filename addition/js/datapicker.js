$(document).ready(function(){  
  $(function() {
    $( "#birthdate" ).datepicker({
//      showOn: "button",
//      buttonImage: "images/calendar.gif",
      buttonImageOnly: true,
      changeMonth: true,
      changeYear: true,
      dateFormat: "yy-mm-dd",
      maxDate: "Now"
    });
  });
  
});