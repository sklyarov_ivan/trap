$(document).ready(function(){
   var send_message_form = $('div[aria-hidden=false] form')

  
    $("#registrate_form").validate({ 
    rules: { 
	      firstname: {
                required: true,
                maxlength: 20
              },
              lastname: {
                required: true,
                maxlength: 20
              },
              email: { 
                        required: true, 
                        email: true,

                        remote: {
                            url: '/ajax/auth/checkemail',
                            type: 'post',
                            data: {
                                email:function(){
                                    return $("#registrate_form p #email").val()}
                            },
                            complete: function(data)
                            {
                             
                            }
                        }

                    },
	      url: { 
	        url: true 
	      }, 
	      password: { 
	        required: true,
                minlength: 5
	      },
              password_confirm: { 
	        required: true,
                equalTo: '#password'
	      },
              birthdate: {
                required: true,
              }
	    },
     messages:
     {
     email: 'wrong email or already registrate'
     },
         
     submitHandler: function(form)
     {
          form.submit(function(event){
           event.preventDefault();
           console.log('registrate');
           console.log($(this).serialize())
           $.ajax({
               url: '/ajax/auth/registrate',
               type: 'POST',
               data: $(this).serialize(),
               dataType: 'json',
               success: function(response)
               {
                   var errors = response.errors
                   for (key in errors)
                       {
                          
                            $('#registrate').prepend(errors[key]+'<br/>').addClass('error')
                               
                       }
                  console.log(response);
                    document.location.href = response.redirect;
                    
                       
               },
               error: function(a,b,c)
               {
                   console.log(a+b+c);
               }
               
            });
          });
         
         
         
         
     }
	    
 });
   $("#login_form").validate({ 
    rules: { 
	      email: {
                  email: true,
                required: true,
              },
              password: {
                required: true,
                minlength: 5
              }
    },
     submitHandler: function(form)
     {
           console.log('login');
          $('#login_form').on('submit',function(event){
           event.preventDefault();
           console.log($(this).serialize())
           $.ajax({
               url: '/ajax/auth/login',
               type: 'POST',
               data: $(this).serialize(),
               dataType: 'json',
               success: function(response)
               {
                   var errors = response.errors
                   console.log(errors);
                   if(response.errors)
                       {
                            $('#login .error').html(errors);
                               
                       }
                       else
                       {
                    document.location.href = response.redirect;

                       }
                    
                       
               },
               error: function(a,b,c)
               {
                   console.log(a+b+c);
               }
            });
               
          });
         
         
         
         
     }
   });
   
   $('#message form').on('submit',function(event){
       event.preventDefault();
       console.log('form click')
   })
   
   
   send_message_form.validate({ 
    rules: { 
	      title: {
                required: true,
                maxlength: 40
              },
              message: {
                required: true
              }
    },
       submitHandler: function(form)
     {
          form.submit(function(event){
           event.preventDefault();
           console.log('message');
           console.log(form.serialize())
           $.ajax({
               url: '/messages/send',
               type: 'POST',
               data: form.serialize(),
               dataType: 'json',
               success: function(response)
               {
                   console.log('send');
//                    document.location.href = document.location.href;
                    
                       
               },
               error: function(a,b,c)
               {
                   console.log(a+b+c);
               }
               
            });
          });
         
         
         
         
     }
   });
   
});